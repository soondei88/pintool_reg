
#include "pin.H"
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <map>

static string firstIMGName = "";
static int rtnCount = 0;
static int regSetCount = 0;
static ADDRINT firstIMGLowAddr = 0;
static ADDRINT firstIMGHighAddr = 0;

static ofstream outFileBasicInfo;
static ofstream outFileOPCODE;
static ofstream outFileDisassemble;
static ofstream outFileCategory;
static ofstream outFileReg;

static map<ADDRINT, OPCODE> opcodeMap;
static map<ADDRINT, string> disassemblyMap;
static map<ADDRINT, INT32> categoryMap;

VOID routine(RTN rtn, VOID *v) {

	if (rtnCount == 0) {
		bool rtnValid = RTN_Valid(rtn);

		if (rtnValid) {
			ADDRINT rtnAddress = RTN_Address(rtn);
			IMG rtnIMG = IMG_FindByAddress(rtnAddress);
			bool rtnIMGValid = IMG_Valid(rtnIMG);

			if (rtnIMGValid) {
				ADDRINT rtnIMGLowAddress = IMG_LowAddress(rtnIMG);
				firstIMGLowAddr = rtnIMGLowAddress;
				ADDRINT rtnIMGHighAddress = IMG_HighAddress(rtnIMG);
				firstIMGHighAddr = rtnIMGHighAddress;
				firstIMGName = IMG_Name(rtnIMG);
				rtnCount++;
			}
		}
	}
}

VOID getOpcode(INS ins) {
	ADDRINT insAddr = INS_Address(ins);
	IMG insIMG = IMG_FindByAddress(insAddr);
	bool insIMGValid = IMG_Valid(insIMG);

	if (insIMGValid) {
		string insIMGName = IMG_Name(insIMG);

		if (insIMGName == firstIMGName) {

			OPCODE insOPCODE = INS_Opcode(ins);
			opcodeMap[insAddr] = insOPCODE;
			string disassemble = INS_Disassemble(ins);
			disassemblyMap[insAddr] = disassemble;

			INT32 cat = INS_Category(ins);
			categoryMap[insAddr] = cat;
		}
	}
}

static bool checkStart = false;
VOID PrintRegisters(ADDRINT insAddr, const CONTEXT * ctxt)
{
	PIN_LockClient();
	IMG insIMG = IMG_FindByAddress(insAddr);
	bool insIMGValid = IMG_Valid(insIMG);

	if (regSetCount == 0) {
		outFileReg << "IMG name : " << firstIMGName << endl;
		outFileReg << " - Low Address : " << hex << firstIMGLowAddr << endl;
		outFileReg << " - High Address : " << hex << firstIMGHighAddr << dec << endl << endl;
		regSetCount++;
	}
	if (insIMGValid) {
		string insIMGName = IMG_Name(insIMG);
		if (insIMGName == firstIMGName) {
			if (insAddr >= firstIMGLowAddr + 76780 && insAddr <= firstIMGLowAddr + 77056) {
				if (insAddr >= firstIMGLowAddr + 76780) {
					checkStart = true;
				}
				//if (insAddr >= firstIMGLowAddr + 77056) {
				//	checkStart = false;
				//}
				if (checkStart) {
					outFileReg << hex << insAddr << dec << endl;
					for (int reg = (int)REG_GR_BASE; reg <= (int)REG_GR_LAST; ++reg)
					{
						// For the integer registers, it is safe to use ADDRINT. But make sure to pass a pointer to it.
						ADDRINT val;
						PIN_GetContextRegval(ctxt, (REG)reg, reinterpret_cast<UINT8*>(&val));
						outFileReg << REG_StringShort((REG)reg) << ": 0x" << hex << val << endl;
					}
					ADDRINT val;
					PIN_GetContextRegval(ctxt, REG_EIP, reinterpret_cast<UINT8*>(&val));
					outFileReg << REG_StringShort(REG_EIP) << ": 0x" << hex << val << endl;
					outFileReg << endl;
				}
			}
		}
	}
	PIN_UnlockClient();
}


// Pin calls this function every time a new instruction is encountered
VOID Instruction(INS ins, VOID *v)
{
	// Insert a call to printip before every instruction, and pass it the IP
	//TRACE_InsertCall(trace, IPOINT_BEFORE, (AFUNPTR)PrintRegisters, IARG_CONST_CONTEXT, IARG_END);
	INS_InsertCall(ins, IPOINT_BEFORE, (AFUNPTR)PrintRegisters, IARG_INST_PTR, IARG_CONST_CONTEXT, IARG_END);
	getOpcode(ins);
}

// This function is called when the application exits
VOID Fini(INT32 code, VOID *v)
{

	int opcodeMapSize = opcodeMap.size();
	int disassemblyMapSize = disassemblyMap.size();
	int categoryMapSize = categoryMap.size();
	int count = 0;

	outFileBasicInfo << "FirstIMG : " << firstIMGName << endl;
	outFileBasicInfo << " - Low address : " << hex << firstIMGLowAddr << endl;
	outFileBasicInfo << " - High address : " << hex << firstIMGHighAddr << endl;

	outFileOPCODE << "{";
	for (auto iter = opcodeMap.begin(); iter != opcodeMap.end(); ++iter) {
		count++;
		outFileOPCODE << '"';
		ADDRINT addr = iter->first;
		outFileOPCODE << hex << addr << dec << '"' << ":";
		OPCODE op = iter->second;
		if(count != opcodeMapSize){
			outFileOPCODE << '"' << hex << op << '"' << "," << endl;
		}
		else {
			outFileOPCODE << '"' << hex << op << '"' << endl;
		}
	}
	count = 0;
	outFileOPCODE << "}";

	outFileDisassemble << "{";
	for (auto iter = disassemblyMap.begin(); iter != disassemblyMap.end(); ++iter) {
		count++;
		outFileDisassemble << '"';
		ADDRINT addr = iter->first;
		outFileDisassemble << hex << addr << dec << '"' << ":";
		string disassembly = iter->second;
		if (count != disassemblyMapSize) {
			outFileDisassemble << '"' << disassembly << '"' << "," << endl;
		}
		else {
			outFileDisassemble << '"' << disassembly << '"' << endl;
		}
	
	}
	count = 0;
	outFileDisassemble << "}";

	outFileCategory << "{";
	for (auto iter = categoryMap.begin(); iter != categoryMap.end(); ++iter) {
		count++;
		outFileCategory << '"';
		ADDRINT addr = iter->first;
		outFileCategory << hex << addr << dec << '"' << ":";
		INT32 cat = iter->second;
		if (count != categoryMapSize) {
			outFileCategory << '"' << cat << '"' << "," << endl;
		}
		else {
			outFileCategory << '"' << cat << '"' << endl;
		}
	}
	outFileCategory << "}";
}


/* ===================================================================== */
/* Print Help Message                                                    */
/* ===================================================================== */

INT32 Usage()
{
	cerr << "This tool collects addresses and opcodes" << endl;
	return -1;
}

/* ===================================================================== */
/* Main                                                                  */
/* ===================================================================== */

int main(int argc, char * argv[])
{

	// Initialize pin
	if (PIN_Init(argc, argv)) {
		return Usage();
	}
	outFileOPCODE.open("result_opcode.json");
	outFileDisassemble.open("result_disassembly.json");
	outFileBasicInfo.open("result_basic_info.txt");
	outFileCategory.open("result_category.json");
	outFileReg.open("result_register.txt");

	// Register rpitome to be called to instrument
	RTN_AddInstrumentFunction(routine, 0);
	// Register Instruction to be called to instrument instructions
	INS_AddInstrumentFunction(Instruction, 0);

	// Register Fini to be called when the application exits
	PIN_AddFiniFunction(Fini, 0);

	//TRACE_AddInstrumentFunction(Trace, 0);

	// Start the program, never returns
	PIN_StartProgram();

	return 0;
}
